# TouchVisualize

#### 介绍
使用objc runtime的API替换UIWindow的sendEvent:方法，实现iOS触控可视化

#### 基本功能
在iOS App内显示触控的位置

#### 使用

1.  添加`TouchVisualize`和`NSSetSafe`两个文件夹到你的项目
2.  标记编译宏 `#define kTouchVisualize 1`

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### Demo介绍
运行起来后打开开关即可显示触控的位置，如图
![单点触控](./imgs/IMG_0799.PNG)
![多点触控](./imgs/IMG_0800.PNG)