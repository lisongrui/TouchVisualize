//
//  ViewController.m
//  TouchDemo
//
//  Created by songruili on 2021/5/18.
//

#import "ViewController.h"
#import "UIWindow+TouchVisualize.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)onStateChange:(UISwitch *)visualSwitch
{
#if kTouchVisualize
    if (visualSwitch.isOn)
    {
        [UIWindow enableTouchVisualize];
    }
    else
    {
        [UIWindow disableTouchVisualize];
    }
#endif
}


@end
