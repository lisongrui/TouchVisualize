//
//  QMTouchVisualize.h
//  TouchDemo
//
//  Created by songruili on 2021/5/18.
//

#ifndef QMTouchVisualize_h
#define QMTouchVisualize_h

#define kTouchVisualize 1

#import "UITouch+TouchVisualize.h"
#import "UIWindow+TouchVisualize.h"

#endif /* QMTouchVisualize_h */
