//
//  UITouch+TouchVisualize.m
//  TouchDemo
//
//  Created by songruili on 2021/5/18.
//

#if kTouchVisualize

#import "UITouch+TouchVisualize.h"
#import <objc/runtime.h>

@implementation UITouch (TouchVisualize)

- (CALayer *)updateVisualizeLayer
{
    if (!self.visualizeLayer)
    {
        CALayer *layer = [CAShapeLayer layer];
        layer.bounds = CGRectMake(0, 0, 44, 44);
        layer.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5].CGColor;
        layer.cornerRadius = 22;
        self.visualizeLayer = layer;
    }
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    self.visualizeLayer.position = [self locationInView:self.window];
    [CATransaction commit];
    return self.visualizeLayer;
}

#pragma mark - Property

- (CALayer *)visualizeLayer
{
    return objc_getAssociatedObject(self, @selector(visualizeLayer));
}

- (void)setVisualizeLayer:(CALayer *)visualizeLayer
{
    objc_setAssociatedObject(self, @selector(visualizeLayer), visualizeLayer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

#endif
