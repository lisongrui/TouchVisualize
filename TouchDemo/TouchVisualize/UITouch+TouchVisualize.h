//
//  UITouch+TouchVisualize.h
//  TouchDemo
//
//  Created by songruili on 2021/5/18.
//

#if kTouchVisualize

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITouch (TouchVisualize)

@property(nonatomic, strong) CALayer *visualizeLayer;

- (CALayer *)updateVisualizeLayer;

@end

NS_ASSUME_NONNULL_END

#endif
