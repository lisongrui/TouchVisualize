//
//  UIWindow+TouchVisualize.h
//  TouchDemo
//
//  Created by songruili on 2021/5/18.
//

#if kTouchVisualize

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIWindow (TouchVisualize)

/// 是否触控可视
@property(nonatomic, assign, class) BOOL isTouchVisualize;

+ (void)enableTouchVisualize;
+ (void)disableTouchVisualize;

@end

NS_ASSUME_NONNULL_END

#endif
